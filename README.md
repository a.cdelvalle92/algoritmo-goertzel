# Algoritmo Goertzel


DESCRIPCIÓN
============

El algoritmo Goertzel es un filtro digital derivado de la transformada discreta de Fourier (DFT) que puede detectar las componentes de frecuencia específica en una señal, sin analizar todo el espectro, resultando en un menor tiempo de ejecución.

AUTORES
========

1.Azucena Camacho Del Valle
2.Manuel Santos Gómez
3.Rubén Perea Roldán

OBJETIVOS
=========
Nos hemos unido en este proyecto para trabajar en el desarrollo del Algoritmo de Groetzel. 
Este proyecto forma parte del desarrollo del estudio previo y elaboración en aula de una práctica
de la asignatura optativa de Procesamiento Digital de Señales. 
En la que trabajamos con un procesador DSP que nos ayuda a ejecutar este tipo de funciones.

Los objetivos a desarrollar son:
* Completar el código aportado por el profesor
* Hacer funcionar correctamente el algoritmo que describió Gerald Goertzel en 1958
* Probar su funcionamiento en el aula mediante la aplicación CodeComposer Studio.

REQUISITOS
===========
* stereo.c COMPLETAR
* sinewaves.c COMPLETAR
* goertzel_algorithm.c COMPLETAR
* main.c MODIFICAR

LICENCIA
=========

Este proyecto está en el dominio público.


